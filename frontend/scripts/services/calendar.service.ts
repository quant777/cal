/*
 * @author aauer 
 * @date 10/12/15
 * @descr 
 */

/// <reference path="../_all.d.ts" />

module calendar {

  export class CalendarSvc implements ICalendar {

    private API_URI :string;

    public static $inject = [
      '$http', 'ApiSvc'
    ];

    constructor(
      private $http: ng.IHttpService,
      private ApiSvc: IApi
    ) {
      if(this.ApiSvc.IsProduction()) {
        this.API_URI = this.ApiSvc.GetApiUri() +  "/Lists/getbytitle('IT-SERVICES Informationen')";
      } else {
        this.API_URI = this.ApiSvc.GetApiUri() +  '/cal';
      }

    }


    GetCalendarEntries(department : string):any {
      var filter : string = "";

      if(this.ApiSvc.IsProduction()) {
        return this.$http.get(this.API_URI + "/items?$filter=((Category eq 'Termine') and (fRecurrence eq 0)) " + filter);
      } else {
        return this.$http.get(this.API_URI + '/cal.json');
      }
    }

    GetUserData():angular.IHttpPromise<any> {
      if(this.ApiSvc.IsProduction()) {
        return this.$http.get(this.ApiSvc.GetApiUri() +  '/SP.UserProfiles.PeopleManager/GetMyProperties');
      } else {
        return this.$http.get(this.API_URI + '/user.json');
      }
    }





  }

}

