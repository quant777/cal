/// <reference path="../_all.d.ts"/>
Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
};
var calendar;
(function (calendar) {
    var CalendarCtrl = (function () {
        function CalendarCtrl(CalendarSvc, test, $log) {
            this.CalendarSvc = CalendarSvc;
            this.test = test;
            this.$log = $log;
            this.events = [];
            this.calendarView = 'week';
            this.calendarTitle = 'IT-Services CAL';
            this.loaded = false;
            var vm = this;
            vm.calendarDay = new Date();
            vm.events = [];
            this.GetUserInfo();
        }
        CalendarCtrl.prototype.GetUserInfo = function () {
            var vm = this;
            this.CalendarSvc.GetUserData().success(function (data) {
                var user = {
                    "firstname": null,
                    "lastname": null,
                    "workEmail": null,
                    "workPhone": null,
                    "department": null
                };
                var properties = data.d.UserProfileProperties.results;
                for (var i = 0; i < properties.length; i++) {
                    var property = properties[i];
                    if (property.Key == "FirstName") {
                        user.firstname = property.Value;
                    }
                    if (property.Key == "LastName") {
                        user.lastname = property.Value;
                    }
                    if (property.Key == "WorkEmail") {
                        user.workEmail = property.Value;
                    }
                    if (property.Key == "WorkPhone") {
                        user.workPhone = property.Value;
                    }
                    if (property.Key == "Department") {
                        user.department = property.Value;
                    }
                }
                vm.$log.debug("User department: " + user.department);
                vm.GetDates(user.department);
            });
        };
        CalendarCtrl.prototype.GetDates = function (department) {
            var vm = this;
            vm.CalendarSvc.GetCalendarEntries(department).success(function (data) {
                var eventsStack = data.d.results;
                eventsStack.forEach(function (event) {
                    var startDate;
                    var endDate;
                    startDate = new Date(event.EventDate);
                    endDate = new Date(event.EndDate);
                    startDate.addHours(-1);
                    endDate.addHours(-1);
                    var e = {
                        title: event.Id + " " + event.Title,
                        type: 'info',
                        startsAt: startDate,
                        endsAt: endDate,
                        editable: false,
                        deletable: false,
                        draggable: false,
                        resizable: false,
                        incrementsBadgeTotal: false,
                        recursOn: 'year',
                        cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
                    };
                    vm.events.push(e);
                });
            });
        };
        CalendarCtrl.$inject = [
            'CalendarSvc',
            'moment',
            '$log'
        ];
        return CalendarCtrl;
    })();
    calendar.CalendarCtrl = CalendarCtrl;
})(calendar || (calendar = {}));
//# sourceMappingURL=calendar.controller.js.map