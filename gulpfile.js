/**
 * Created by Florian on 12.12.2015.
 */



var gulp = require('gulp'),
    less = require('gulp-less'), // compiles less to CSS
    minify = require('gulp-minify-css'), // minifies CSS
    concat = require('gulp-concat'),
    git = require('gulp-git'),
    changed = require('gulp-changed'),
    uglify = require('gulp-uglify'), // minifies JS
    livereload = require('gulp-livereload'),
    serve = require('gulp-serve'),
    sourcemaps = require('gulp-sourcemaps'),
    ngAnnotate = require('gulp-ng-annotate'),
    order = require('gulp-order'),
    rename = require('gulp-rename');

var appname = "inventar";

// Paths variables
var paths = {
    'dev': {
        'less': 'frontend/less',
        'js': 'frontend/scripts',
        'html': 'frontend/partials'

    },
    'assets': {
        'css': 'public/css',
        'js': 'public/js',
        'html': 'public/'
    }

};



gulp.task('frontendjs', function(){
    return gulp.src([paths.dev.js+'/interfaces/*.js', paths.dev.js+'/filters/*.js', paths.dev.js+'/services/*.js', paths.dev.js+'/directives/*.js', paths.dev.js+'/controllers/*.js', paths.dev.js+'/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(ngAnnotate())
        //.pipe(uglify())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.assets.js))
        //.pipe(git.commit("auto commit", { args: '--amend -s'    }))
        //.pipe(git.push('origin', 'master', function (err) { if (err) throw err; }))
        .pipe(livereload())
});

gulp.task('html', function() {
    gulp.src(paths.assets.html + '/**/*.html')
        .pipe(changed(paths.assets.html))
        .pipe(livereload())
});

gulp.task('less', function() {
    return gulp.src([paths.dev.less + '/*.less']) // get file
        .pipe(concat('style.min.css'))
        .pipe(less())
        .pipe(minify({keepSpecialComments:0}))
        .pipe(gulp.dest(paths.assets.css))
        .pipe(livereload())
});


gulp.task('watch', function() {
    livereload.listen();
    gulp.watch([paths.dev.less + '/**/*.less'], ['less']);
    gulp.watch(paths.dev.js + '/**/*.js', ['frontendjs']);
    gulp.watch(paths.assets.html + '/**/*.html', ['html'])
});

gulp.task('default', ['watch']);


