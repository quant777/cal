/*
 * @author aauer
 * @date 10/12/15
 * @descr
 */
/// <reference path="../_all.d.ts" />
var calendar;
(function (calendar) {
    var CalendarSvc = (function () {
        function CalendarSvc($http, ApiSvc) {
            this.$http = $http;
            this.ApiSvc = ApiSvc;
            if (this.ApiSvc.IsProduction()) {
                this.API_URI = this.ApiSvc.GetApiUri() + "/Lists/getbytitle('IT-SERVICES Informationen')";
            }
            else {
                this.API_URI = this.ApiSvc.GetApiUri() + '/cal';
            }
        }
        CalendarSvc.prototype.GetCalendarEntries = function (department) {
            var filter = "";
            if (this.ApiSvc.IsProduction()) {
                return this.$http.get(this.API_URI + "/items?$filter=((Category eq 'Termine') and (fRecurrence eq 0)) " + filter);
            }
            else {
                return this.$http.get(this.API_URI + '/cal.json');
            }
        };
        CalendarSvc.prototype.GetUserData = function () {
            if (this.ApiSvc.IsProduction()) {
                return this.$http.get(this.ApiSvc.GetApiUri() + '/SP.UserProfiles.PeopleManager/GetMyProperties');
            }
            else {
                return this.$http.get(this.API_URI + '/user.json');
            }
        };
        CalendarSvc.$inject = [
            '$http', 'ApiSvc'
        ];
        return CalendarSvc;
    })();
    calendar.CalendarSvc = CalendarSvc;
})(calendar || (calendar = {}));
//# sourceMappingURL=calendar.service.js.map