/// <reference path="_all.d.ts"/>



/**
 * The main file, all the routing + dependency loading is here
 *
 * @type {angular.module}
 **/

module calendar {
  var app = angular.module("CalendarApp", ['ui.router', 'mwl.calendar', 'ui.bootstrap'])

    /*
     *
     * Controllers
     *
     */

    .controller('CalendarCtrl', CalendarCtrl)


    /*
     *
     * Services
     *
     */

    .service('ApiSvc', ApiSvc)
    .service('CalendarSvc', CalendarSvc)


    /*
     *
     * Configuration
     *
     */


    .config( function($logProvider, $stateProvider, $urlRouterProvider, $locationProvider, calendarConfigProvider) {
      $logProvider.debugEnabled(true);
      $locationProvider.html5Mode(false);
      $urlRouterProvider.otherwise("/");

      calendarConfigProvider.setDateFormatter('moment');

      moment.locale("de-at",{
        "week": {
          "dow": 1
        }
      });






      // Routing
      $stateProvider
        .state('calendar',    { url: '/',         templateUrl: 'partials/calendar/index.html',    controller: 'CalendarCtrl as main' })
    });
}

