/*
 * @author aauer 
 * @date 10/12/15
 * @descr 
 */

/// <reference path="../_all.d.ts" />

module calendar {

  export class ApiSvc implements IApi {

    private API_URI : string;

    public static $inject = [
      '$http'
    ];

    constructor(private $http : ng.IHttpService) {
      this.API_URI = this.IsProduction() ? "https://swa.wu.ac.at/Serviceeinrichtungen/it-services/_api" : "http://localhost/api";
      $http.defaults.headers.common.Accept = 'application/json;odata=verbose';


    }

    GetApiUri():string {
      return this.API_URI;
    }

    IsProduction():boolean {
      return (window.location.host == 'swa.wu.ac.at');
    }

  }

}