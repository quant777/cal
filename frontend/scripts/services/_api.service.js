/*
 * @author aauer
 * @date 10/12/15
 * @descr
 */
/// <reference path="../_all.d.ts" />
var calendar;
(function (calendar) {
    var ApiSvc = (function () {
        function ApiSvc($http) {
            this.$http = $http;
            this.API_URI = this.IsProduction() ? "https://swa.wu.ac.at/Serviceeinrichtungen/it-services/_api" : "http://localhost/api";
            $http.defaults.headers.common.Accept = 'application/json;odata=verbose';
        }
        ApiSvc.prototype.GetApiUri = function () {
            return this.API_URI;
        };
        ApiSvc.prototype.IsProduction = function () {
            return (window.location.host == 'swa.wu.ac.at');
        };
        ApiSvc.$inject = [
            '$http'
        ];
        return ApiSvc;
    })();
    calendar.ApiSvc = ApiSvc;
})(calendar || (calendar = {}));
//# sourceMappingURL=_api.service.js.map