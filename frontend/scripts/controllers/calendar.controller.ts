/// <reference path="../_all.d.ts"/>



Date.prototype.addHours = function(h) {
  this.setTime(this.getTime() + (h*60*60*1000));
  return this;
};

module calendar {

  export class CalendarCtrl {

    public events = [];
    private moment;
    private calendarView = 'week';
    private calendarTitle = 'IT-Services CAL';
    private calendarDay;
    private loaded = false;

    public static $inject = [
      'CalendarSvc',
      'moment',
      '$log'
    ];



    constructor(private CalendarSvc: ICalendar, private test: moment.MomentStatic, private $log: ng.ILogService){
      var vm = this;
      vm.calendarDay = new Date();
      vm.events = [];
      this.GetUserInfo();
    }

    public GetUserInfo() :void  {
      var vm = this;
      this.CalendarSvc.GetUserData().success(function (data) {
        var user = {
          "firstname": null,
          "lastname": null,
          "workEmail": null,
          "workPhone": null,
          "department": null
        };
        var properties = data.d.UserProfileProperties.results;
        for (var i = 0; i < properties.length; i++) {
          var property = properties[i];
          if (property.Key == "FirstName") {
            user.firstname = property.Value;
          }
          if (property.Key == "LastName") {
            user.lastname = property.Value;
          }
          if (property.Key == "WorkEmail") {
            user.workEmail = property.Value;
          }
          if (property.Key == "WorkPhone") {
            user.workPhone = property.Value;
          }
          if (property.Key == "Department") {
            user.department = property.Value;
          }
        }
        vm.$log.debug("User department: " + user.department);
        vm.GetDates(user.department);
      })
    }

    public GetDates(department : string) : void {
      var vm = this;
      vm.CalendarSvc.GetCalendarEntries(department).success(function (data) {
        var eventsStack = data.d.results;
        eventsStack.forEach(function (event) {
          var startDate : Date;
          var endDate : Date;
          startDate = new Date(event.EventDate);
          endDate = new Date(event.EndDate);
          startDate.addHours(-1);
          endDate.addHours(-1);
          var e  = {
            title: event.Id + " " + event.Title,
            type: 'info',
            startsAt: startDate,
            endsAt: endDate,
            editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
            deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
            draggable: false, //Allow an event to be dragged and dropped
            resizable: false, //Allow an event to be resizable
            incrementsBadgeTotal: false, //If set to false then will not count towards the badge total amount on the month and year view
            recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
            cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
          };
          vm.events.push(e);
        });
      });
    }


  }
}

