/*
 * @author aauer 
 * @date 10/12/15
 * @descr 
 */

/// <reference path="../_all.d.ts" />


module calendar {
  export interface ICalendar {
    GetCalendarEntries (department:string): any;

    GetUserData () : ng.IHttpPromise<any>;
  }
}
